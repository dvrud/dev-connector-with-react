const express = require("express");
const connectDB = require("./config/db");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();

app.use(cors());
// Middleware for bodyParser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Connecting the mongo Database
connectDB();

// Bringing in the routes.
app.use("/api/posts", require("./routes/api/posts"));
app.use("/api/users", require("./routes/api/users"));
app.use("/api/auth", require("./routes/api/auth"));
app.use("/api/profile", require("./routes/api/profile"));

// Setting up the port
const PORT = process.env.PORT || 4900;
app.listen(PORT, () => {
  console.log(`Server started on ${PORT}`);
});
