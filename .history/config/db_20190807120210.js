const mongoose = require("mongoose");
const config = require("config");
const dbString = config.get("mongoURI");

const connectDB = async () => {
  try {
    await mongoose.connect(dbString, {
      useNewUrlParser: true,
      useCreateIndex: true
    });

    console.log(`MongoDB connected on ${dbString}`);
  } catch (err) {
    console.log(err.message);
    // Exits process with the failure
    process.exit(1);
  }
};

module.exports = connectDB;
