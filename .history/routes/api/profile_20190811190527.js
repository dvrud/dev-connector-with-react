const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const profiles = require("../../models/Profile");
const users = require("../../models/User");
const request = require("request");
const config = require("config");
const { check, validationResult } = require("express-validator");

router.get("/me", auth, async (req, res) => {
  try {
    const profile = await profiles
      .findOne({ user: req.user.id })
      .populate("user", ["name", "avatar"]);

    if (!profile) {
      return res.status(400).json({ msg: "There is no profile for this user" });
    }

    res.json(profile);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

// add profile or update profile route
router.post(
  "/",
  [
    auth,
    [
      check("status", "Status is required")
        .not()
        .isEmpty(),
      check("skills", "Skills is required")
        .not()
        .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const {
      company,
      website,
      location,
      bio,
      status,
      githubUsername,
      skills,
      youtube,
      facebook,
      twitter,
      instagram,
      linkedin
    } = req.body;
    // Build profile object
    const profileField = {};
    profileField.user = req.user.id;
    if (company) profileField.company = company;
    if (website) profileField.website = website;
    if (location) profileField.location = location;
    if (bio) profileField.bio = bio;
    if (status) profileField.status = status;
    if (githubUsername) profileField.githubUsername = githubUsername;

    if (skills) {
      profileField.skills = skills.split(",").map(skill => skill.trim());
    }

    // Build social object
    profileField.social = {};
    if (youtube) profileField.social.youtube = youtube;
    if (twitter) profileField.social.twitter = twitter;
    if (facebook) profileField.social.facebook = facebook;
    if (linkedin) profileField.social.linkedin = linkedin;
    if (instagram) profileField.social.instagram = instagram;

    try {
      let profile = await profiles.findOne({ user: req.user.id });
      if (profile) {
        profile = await profiles.findOneAndUpdate(
          { user: req.user.id },
          { $set: profileField },
          { new: true }
        );
        return res.json(profile);
      }

      profile = new profiles(profileField);

      let createdProfile = await profile.save();
      res.json(profile);
    } catch (err) {
      console.error(err.message);
      res.status(500).send("server Error");
    }
  }
);

// Route for getting all the profiles
router.get("/getProfiles", async (req, res) => {
  try {
    const allProfiles = await profiles
      .find()
      .populate("user", ["name", "avatar"]);

    res.json(allProfiles);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

// Route for getting the single profile of the user
router.get("/getSingleProfile/:userid", async (req, res) => {
  try {
    const userProfile = await profiles
      .findOne({ user: req.params.userid })
      .populate("user", ["name", "avatar"]);
    if (!userProfile) {
      return res.status(400).json({ msg: "Profile not found" });
    }
    res.json(userProfile);
  } catch (err) {
    console.error(err.message);
    if (err.kind == "ObjectId") {
      return res.status(400).json({ msg: "Profile not found" });
    }
    res.status(500).send("Server Error");
  }
});

// Route for deleting the user and its profile
router.delete("/deleteProfile", auth, async (req, res) => {
  try {
    // Removes the users profile
    await profiles.findOneAndRemove({ user: req.user.id });
    // Removes the user
    await users.findOneAndRemove({ _id: req.user.id });
    res.json({ msg: "User and its profile is successfully deleted" });
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

// Route for adding the experience
router.post(
  "/addExperience",
  [
    auth,
    [
      check("title", "Title is required")
        .not()
        .isEmpty(),
      check("company", "Company is required")
        .not()
        .isEmpty(),
      check("from", "From date is required")
        .not()
        .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const {
      title,
      company,
      location,
      from,
      to,
      current,
      description
    } = req.body;

    const newExperience = {
      title,
      company,
      location,
      from,
      to,
      current,
      description
    };

    try {
      const userProfile = await profiles.findOne({ user: req.user.id });
      userProfile.experience.unshift(newExperience);

      await userProfile.save();
      res.json(userProfile);
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server Error");
    }
  }
);

router.delete("/deleteExperience/:id", auth, async (req, res) => {
  try {
    const userProfile = await profiles.findOne({ user: req.user.id });

    const removeIndex = userProfile.experience.map(item =>
      item.id.indexOf(req.params.id)
    );

    userProfile.experience.splice(removeIndex, 1);

    await userProfile.save();

    res.json(userProfile);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

router.post(
  "/addEducation",
  [
    auth,
    [
      check("school", "School is required")
        .not()
        .isEmpty(),
      check("degree", "Degree is required")
        .not()
        .isEmpty(),
      check("from", "From date is required")
        .not()
        .isEmpty(),
      check("fieldOfStudy", "Study is required")
        .not()
        .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const {
      school,
      degree,
      fieldOfStudy,
      from,
      to,
      current,
      description
    } = req.body;

    const newEducation = {
      school,
      degree,
      fieldOfStudy,
      from,
      to,
      current,
      description
    };

    try {
      const userProfile = await profiles.findOne({ user: req.user.id });
      userProfile.education.unshift(newEducation);

      await userProfile.save();
      res.json(userProfile);
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server Error");
    }
  }
);

router.delete("/deleteEducation/:id", auth, async (req, res) => {
  try {
    const userProfile = await profiles.findOne({ user: req.user.id });

    const removeIndex = userProfile.education.map(item =>
      item.id.indexOf(req.params.id)
    );

    userProfile.education.splice(removeIndex, 1);

    await userProfile.save();

    res.json(userProfile);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

router.get("/github/:username", (req, res) => {
  try {
    const options = {
      uri: `https://api.github.com/users/${
        req.params.username
      }/repos?per_page=5&sort=created:asc&client_id=${config.get(
        "Client_Id"
      )}&client_secret=${config.get("Client_Secret")}`,
      method: "GET",
      headers: { "user-agent": "node.js" }
    };
  } catch (err) {
    console.err(err.message);
    res.status(500).send("Server Error");
  }
});
module.exports = router;
