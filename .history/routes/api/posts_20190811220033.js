const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");

const { check, validationResult } = require("express-validator");
// bringing the model
const posts = require("../../models/posts");
const users = require("../../models/User");
const profiles = require("../../models/Profile");

// Routes
router.post(
  "/addPost",
  [
    auth,
    [
      check("text", "Body of the post is required")
        .not()
        .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      const user = await users.findById(req.user.id).select("-password");

      const newPost = {
        text: req.body.text,
        name: user.name,
        avatar: user.avatar,
        user: req.user.id
      };

      const post = await new posts(newPost).save();

      res.json(post);
    } catch (err) {
      console.err(err.message);
      res.status(500).send("Server Error");
    }
  }
);

// Getting the posts
router.get("/getAllPosts", auth, async (req, res) => {
  try {
    const post = await posts.find().sort({ date: -1 });

    res.json(post);
  } catch (err) {
    console.err(err.message);
    res.status(500).send("Server Error");
  }
});

router.get("/getSinglePost/:id", auth, async (req, res) => {
  try {
    const post = await posts.findById(req.params.id);
    if (!post) {
      return res.status(404).json({ msg: "Post not found" });
    }
    res.json(post);
  } catch (err) {
    console.err(err.message);
    if (err.kind === "ObjectId") {
      return res.status(404).json({ msg: "Post not found" });
    }
    res.status(500).send("Server Error");
  }
});

router.delete("/deleteSinglePost/:id", auth, async (req, res) => {
  try {
    const post = await posts.findById(req.params.id);

    if (!post) {
      return res.status(401).json({ msg: "Post not authorized" });
    }

    // User check
    if (post.user.toString() !== req.user.id) {
      return res.status(401).json({ msg: "User not authorized" });
    }
    await post.remove();
    res.json({ msg: "Post removed" });
  } catch (err) {
    console.err(err.message);
    if (err.kind === "ObjectId") {
      return res.status(404).json({ msg: "Post not found" });
    }
    res.status(500).send("Server Error");
  }
});

router.post("/addLike/:id", auth, async (req, res) => {
  try {
    const post = await posts.findById(req.params.id);
    if (
      post.likes.filter(like => like.user.toString() === req.user.id).length > 0
    ) {
      return res.status(400).json({ msg: "Post already likes" });
    }

    post.likes.unshift({ user: req.user.id });

    await post.save();

    res.json(post.likes);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
  const post = await posts.findOne({ _id: req.params.id });
});

router.post("/addUnlike/:id", auth, async (req, res) => {
  try {
    const post = await posts.findById(req.params.id);
    if (
      (post.likes.filter(
        like => like.user.toString() === req.user.id
      ).length = 0)
    ) {
      return res.status(400).json({ msg: "Post has not yet liked" });
    }

    const removeIndex = post.likes
      .map(like => like.user.toString())
      .indexOf(req.user.id);

    post.likes.splice(removeIndex, 1);
    await post.save();

    res.json(post.likes);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
  const post = await posts.findOne({ _id: req.params.id });
});
module.exports = router;
