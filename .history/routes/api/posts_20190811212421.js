const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");

const { check, validationResult } = require("express-validator");
// bringing the model
const posts = require("../../models/posts");
const users = require("../../models/User");
const profiles = require("../../models/Profile");

// Routes
router.post(
  "/addPost",
  [
    auth,
    [
      check("text", "Body of the post is required")
        .not()
        .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      const user = await users.findById(req.user.id).select("-password");

      const newPost = {
        text: req.body.text,
        name: user.name,
        avatar: user.avatar,
        user: req.user.id
      };

      const post = await new posts(newPost).save();

      res.json(post);
    } catch (err) {
      console.err(err.message);
      res.status(500).send("Server Error");
    }
  }
);

// Getting the posts
router.get("/getAllPosts", auth, async (req, res) => {
  try {
  } catch (err) {
    console.err(err.message);
    res.status(500).send("Server Error");
  }
  const post = await posts.find();

  res.json();
});
module.exports = router;
