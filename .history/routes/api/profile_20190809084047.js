const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const profiles = require("../../models/Profile");
const users = require("../../models/User");
const { check, validationResult } = require("express-validator");

router.get("/me", auth, async (req, res) => {
  try {
    const profile = await profiles
      .findOne({ user: req.user.id })
      .populate("user", ["name", "avatar"]);

    if (!profile) {
      return res.status(400).json({ msg: "There is no profile for this user" });
    }

    res.json(profile);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

// add profile or update profile route
router.post(
  "/",
  [
    auth,
    [
      check("status", "Status is required")
        .not()
        .isEmpty(),
      check("skills", "Skills is required")
        .not()
        .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const {
      company,
      website,
      location,
      bio,
      status,
      githubUsername,
      skills,
      youtube,
      facebook,
      twitter,
      instagram,
      linkedin
    } = req.body;

    // Build profile object
    const profileField = {};
    profileField.user = req.user.id;
    if (company) profileField.company = company;
    if (website) profileField.company = website;
    if (location) profileField.company = location;
    if (bio) profileField.company = bio;
    if (status) profileField.company = status;
    if (githubUsername) profileField.company = githubUsername;

    if (skills) {
      profileField.skills = skills.split(",").map(skill => skill.trim());
    }
  }
);
module.exports = router;
