const express = require("express");
const router = express.Router();

// bringing the model
const post = require("../../models/posts");
router.get("/", (req, res) => {
  res.send("This is the posts Route");
});

module.exports = router;
