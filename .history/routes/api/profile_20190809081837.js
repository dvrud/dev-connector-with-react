const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const profiles = require("../../models/Profile");
const users = require("../../models/User");
router.get("/", async (req, res) => {
  try {
    const profile = await profiles
      .findOne({ user: req.user._id })
      .populate("user", ["name", "avatar"]);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

module.exports = router;
