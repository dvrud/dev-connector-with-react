const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const users = require("../../models/User");
router.get("/", auth, async (req, res) => {
  console.log(req.user);
  try {
    const user = await users.findById(req.user.id).select("-password");
    res.json(user);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

module.exports = router;
