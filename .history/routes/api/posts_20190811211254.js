const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");

const { check, validationResult } = require("express-validator");
// bringing the model
const posts = require("../../models/posts");
const users = require("../../models/User");
const profiles = require("../../models/Profile");

// Routes
router.post(
  "/",
  [
    auth,
    [
      check("text", "Body of the post is required")
        .not()
        .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const user = await users.findOne({ _id: req.user.id });
  }
);

module.exports = router;
