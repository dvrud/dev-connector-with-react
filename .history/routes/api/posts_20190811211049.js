const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const { check, validationResult } = require("express-validator");
// bringing the model
const post = require("../../models/posts");

// Routes
router.post(
  "/",
  [
    auth,
    [
      check("text", "Body of the post is required")
        .not()
        .isEmpty()
    ]
  ],
  async (req, res) => {
    res.send("This is the posts Route");
  }
);

module.exports = router;
