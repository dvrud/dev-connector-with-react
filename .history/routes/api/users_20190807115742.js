const express = require("express");
const router = express.Router();

const { check, validationResult } = require("express-validator");

// Bringing in the models
const user = require("../../models/User");
router.post(
  "/",
  [
    check("name", "Name is Requried")
      .not()
      .isEmpty(),
    check("email", "Please include a valid Email").isEmail(),
    check(
      "password",
      "Please enter a password with 6 or more characters"
    ).isLength({ min: 6 })
  ],
  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { name, email, password } = req.body;
    // See if user exists

    // Get users gravatar

    // Encrypt the password

    // Return the jsonwebtoken
    res.send("This is the user Route");
  }
);

module.exports = router;
