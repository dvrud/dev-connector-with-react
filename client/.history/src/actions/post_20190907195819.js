import {
  GET_POSTS,
  POST_ERROR,
  UPDATE_LIKES,
  DELETE_POST,
  ADD_POST,
  GET_POST,
  ADD_COMMENT,
  REMOVE_COMMENT
} from "./types";
import axios from "axios";
import { setAlert } from "./alert";

// Function to get the posts
export const getPosts = () => async dispatch => {
  try {
    const res = await axios.get("/api/posts/getAllPosts");

    dispatch({
      type: GET_POSTS,
      payload: res.data
    });
  } catch (error) {
    dispatch({
      type: POST_ERROR,
      payload: { msg: error.response.statusText, status: error.response.status }
    });
  }
};

// Addlike
export const addLike = postId => async dispatch => {
  try {
    const res = await axios.post(`/api/posts/addLike/${postId}`);

    dispatch({
      type: UPDATE_LIKES,
      payload: { postId, likes: res.data }
    });
  } catch (error) {
    dispatch({
      type: POST_ERROR,
      payload: { msg: error.response.statusText, status: error.response.status }
    });
  }
};

// RemoveLike
export const removeLike = postId => async dispatch => {
  try {
    const res = await axios.post(`/api/posts/addunLike/${postId}`);

    dispatch({
      type: UPDATE_LIKES,
      payload: { postId, likes: res.data }
    });
  } catch (error) {
    dispatch({
      type: POST_ERROR,
      payload: { msg: error.response.statusText, status: error.response.status }
    });
  }
};

// DeletePost
export const deletePost = postId => async dispatch => {
  try {
    await axios.delete(`/api/posts/deleteSinglePost/${postId}`);

    dispatch({
      type: DELETE_POST,
      payload: postId
    });

    dispatch(setAlert("Post has been deleted successfully", "success"));
  } catch (error) {
    dispatch({
      type: POST_ERROR,
      payload: { msg: error.response.statusText, status: error.response.status }
    });
  }
};

// Adding the post
export const addPost = formData => async dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };
  try {
    const res = await axios.post(`/api/posts/addPost`, formData);

    dispatch({
      type: ADD_POST,
      payload: res.data
    });

    dispatch(setAlert("Post created successfully", "success"));
  } catch (error) {
    dispatch({
      type: POST_ERROR,
      payload: { msg: error.response.statusText, status: error.response.status }
    });
  }
};

// Getting single post
export const getPost = postId => async dispatch => {
  try {
    const res = await axios.get(`/api/posts/getSinglePost/${postId}`);

    dispatch({
      type: GET_POST,
      payload: res.data
    });
  } catch (error) {
    dispatch({
      type: POST_ERROR,
      payload: { msg: error.response.statusText, status: error.response.status }
    });
  }
};
