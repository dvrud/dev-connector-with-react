import { GET_POSTS, POST_ERROR, UPDATE_LIKES } from "./types";
import axios from "axios";
// import { setAlert } from "./alert";

// Function to get the posts
export const getPosts = () => async dispatch => {
  try {
    const res = await axios.get("/api/posts/getAllPosts");

    dispatch({
      type: GET_POSTS,
      payload: res.data
    });
  } catch (error) {
    dispatch({
      type: POST_ERROR,
      payload: { msg: error.response.statusText, status: error.response.status }
    });
  }
};

// Addlike
export const addLike = postId => async dispatch => {
  try {
    const res = await axios.get(`/api/posts/addLike/${postId}`);

    dispatch({
      type: UPDATE_LIKES,
      payload: { postId, likes: res.data }
    });
  } catch (error) {
    dispatch({
      type: POST_ERROR,
      payload: { msg: error.response.statusText, status: error.response.status }
    });
  }
};

// RemoveLike
export const removeLike = postId => async dispatch => {
  try {
    const res = await axios.get(`/api/posts/addunLike/${postId}`);

    dispatch({
      type: UPDATE_LIKES,
      payload: { postId, likes: res.data }
    });
  } catch (error) {
    dispatch({
      type: POST_ERROR,
      payload: { msg: error.response.statusText, status: error.response.status }
    });
  }
};
