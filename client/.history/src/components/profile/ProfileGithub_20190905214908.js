import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getGithubRepos } from "../../actions/profile";
import Spinner from "../layout/Spinner";
const ProfileGithub = ({ username, getGithubRepos, repos }) => {
  return <div></div>;
};

ProfileGithub.propTypes = {
  getGithubRepos: PropTypes.func.isRequired,
  repos: PropTypes.array.isRequired,
  username: PropTypes.string.isRequired
};

const mapStateToProps = state => ({
  repos: state.profile.repos
});
export default connect(
  mapStateToProps,
  {
    getGithubRepos
  }
)(ProfileGithub);
